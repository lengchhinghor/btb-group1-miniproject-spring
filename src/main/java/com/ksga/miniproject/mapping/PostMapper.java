package com.ksga.miniproject.mapping;

import com.ksga.miniproject.dto.post.PostDto;
import com.ksga.miniproject.dto.post.PostRequest;
import com.ksga.miniproject.model.post.Post;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface PostMapper {

//  @Mapping(target = "category",expression = "java(article.getCategory())")
  PostDto mapToDto(Post post);


//  @Mapping(source = "categoryId",target = "category.id")
  Post mapRequestToModel(PostRequest request);

}
