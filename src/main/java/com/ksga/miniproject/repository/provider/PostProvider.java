package com.ksga.miniproject.repository.provider;

import com.ksga.miniproject.model.post.Post;
import com.ksga.miniproject.utilities.Paging;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;


public class PostProvider {


  public String findAll(@Param("paging") Paging paging){

    return new SQL(){{
      SELECT("*");
      FROM("post");
      ORDER_BY("post.id desc  limit #{paging.limit} offset #{paging.offset}");
    }}.toString();
  }
  public String findOne(@Param("id") int id){
    return new SQL(){{
      SELECT("*");
      FROM("post");
      WHERE("post.id = #{id}");
    }}.toString();
  }

  public String insert(Post post){
    return new SQL(){{
      INSERT_INTO("post");
      VALUES("id,username,user_id,image,n_of_like,caption,owner","#{post.id},#{post.username},#{post.user_id},#{post.image},#{post.n_of_like},#{post.caption},#{post.owner}");
    }}.toString();
  }
  public String countAll(){
    return new SQL(){{
      SELECT("count(id)");
      FROM("post");
    }}.toString();
  }

  public String update(Integer id,Post post){
    return new SQL(){{
      UPDATE("post");
      SET("username=#{post.username},user_id=#{post.user_id},n_of_like=#{post.n_of_like},caption=#{post.caption},owner=#{post.owner}");
      WHERE("post.id=#{id}");
    }}.toString();
  }
  public String delete(Integer id){
    return new SQL(){{
      DELETE_FROM("post");
      WHERE("post.id=#{id}");
    }}.toString();
  }





}
