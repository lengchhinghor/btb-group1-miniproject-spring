package com.ksga.miniproject.repository;


import com.ksga.miniproject.model.post.Post;
import com.ksga.miniproject.repository.provider.PostProvider;
import com.ksga.miniproject.utilities.Paging;
import java.util.List;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository {
  @SelectProvider(type = PostProvider.class,method = "findAll")

  public List<Post> findAll(@Param("paging")Paging paging);

  @InsertProvider(type = PostProvider.class,method = "insert")
  public boolean insert(@Param("post") Post post);

  @SelectProvider(type = PostProvider.class,method = "findOne")
//  @ResultMap("articleMapping")
  public Post findOne(@Param("id") Integer id);

  @Select("SELECT MAX(post.id) FROM post;")
  public Post findLastArticle(Post post);

  @SelectProvider(type = PostProvider.class,method = "countAll")
  public int countAllRecord();

  @UpdateProvider(type = PostProvider.class,method = "update")
  public boolean update(Integer id, Post post);

  @DeleteProvider(type = PostProvider.class,method = "delete")
  public boolean delete(@Param("id") Integer id);



}
