package com.ksga.miniproject.service;


import com.ksga.miniproject.dto.post.PostDto;
import com.ksga.miniproject.model.post.Post;
import com.ksga.miniproject.utilities.Paging;
import java.util.List;



public interface PostService {

  public List<PostDto> findAll(Paging paging);
  public boolean insert(Post post);
  public PostDto findOne(Integer id);
  public Post update(Integer id, Post post);
  public Post findLastArticle(Post post);
  public boolean delete(Integer id);

}
