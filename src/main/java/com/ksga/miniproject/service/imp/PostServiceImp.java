package com.ksga.miniproject.service.imp;

import com.ksga.miniproject.dto.post.PostDto;
import com.ksga.miniproject.mapping.PostMapper;
import com.ksga.miniproject.model.post.Post;
import com.ksga.miniproject.repository.PostRepository;
import com.ksga.miniproject.service.PostService;
import com.ksga.miniproject.utilities.Paging;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImp implements PostService {
private final PostRepository repository;
private PostMapper postMapper;
ModelMapper mapper = new ModelMapper();
@Autowired
  public PostServiceImp(PostRepository repository,
                        PostMapper postMapper) {
    this.repository = repository;
    this.postMapper = postMapper;
  }

  @Override
  public List<PostDto> findAll(Paging paging) {
  paging.setTotalCount(repository.countAllRecord());
  List<PostDto> dtoList = new ArrayList<>();
  for(Post a: repository.findAll(paging)){
    PostDto dto = mapper.map(a, PostDto.class);
    dtoList.add(dto);
  }
    return dtoList;
  }

  @Override
  public boolean insert(Post post) {
      return repository.insert(post);
  }

  @Override
  public PostDto findOne(Integer id) {
  PostDto dto = mapper.map(repository.findOne(id), PostDto.class);
    return dto;
  }

  @Override
  public Post update(Integer id, Post post) {
  if(repository.update(id,post)){
    return repository.findOne(id);
  }
    else
      return null;
  }

  @Override
  public Post findLastArticle(Post post) {
    return repository.findLastArticle(post);
  }

  @Override
  public boolean delete(Integer id) {
    return repository.delete(id);
  }


}
