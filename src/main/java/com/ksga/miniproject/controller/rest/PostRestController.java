package com.ksga.miniproject.controller.rest;

import com.ksga.miniproject.dto.post.PostDto;
import com.ksga.miniproject.dto.post.PostRequest;
import com.ksga.miniproject.model.post.Post;
import com.ksga.miniproject.service.PostService;
import com.ksga.miniproject.utilities.Paging;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/posts")
public class PostRestController {

  private final PostService postService;

@Autowired
  public PostRestController(
      PostService postService) {
    this.postService = postService;
  }

  @PostMapping
  public ResponseEntity<Map<String, Object>> insertPost(@RequestBody Post post){
  Map<String, Object> response = new HashMap<>();

  response.put("message","successfully create");
  System.out.println(postService.findLastArticle(post));
  response.put("data", postService.insert(post));
  response.put("status", HttpStatus.CREATED);
  return ResponseEntity.ok(response);
  }

  @GetMapping
  public ResponseEntity<Map<String,Object>> findAll(Paging paging){
  Map<String, Object> response = new HashMap<>();
  response.put("data",postService.findAll(paging));
  response.put("paginate",paging);
  response.put("message","successfully fetched");
  response.put("status", HttpStatus.valueOf(200));
  return ResponseEntity.ok(response);
  }
  @GetMapping("/{id}")
  public PostDto findById(@PathVariable Integer id){
    return postService.findOne(id);
  }

  @PutMapping("/{id}")
  public Post updateById(@PathVariable Integer id, @RequestBody Post post){
   return postService.update(id,post);
  }

  @DeleteMapping("/{id}")
  public String delete(@PathVariable Integer id) {
    if(postService.delete(id)){
      return "Data has been deleted!";
    }else
    return "No record Found!";
  }




}
