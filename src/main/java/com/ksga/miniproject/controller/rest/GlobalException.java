package com.ksga.miniproject.controller.rest;

import com.ksga.miniproject.utilities.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalException {
  @ExceptionHandler(Exception.class)
  public ResponseEntity<ApiError> handleException() {
    ApiError apiError = new ApiError();
    apiError.setMessage("Fail to request");
    apiError.setHttpStatus(HttpStatus.NOT_FOUND);
    return new ResponseEntity<>(apiError, HttpStatus.NOT_FOUND);
  }
}
