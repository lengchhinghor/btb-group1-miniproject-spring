package com.ksga.miniproject.dto.post;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostDto {
  private int id;
  private String username;
  private int user_id;
  private String image;
  private String n_of_like;
  private String caption;
  private Boolean owner;
}
