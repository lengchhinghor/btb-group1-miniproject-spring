package com.ksga.miniproject.dto.post;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Setter
@Getter
public class PostRequest {
    private int id;
    private String username;
    private int user_id;
    private String image;
    private String n_of_like;
    private String caption;
    private Boolean owner;

}
