package com.ksga.miniproject.model.post;

import lombok.*;

@Data
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Post {
    private int id;
    private String username;
    private int user_id;
    private String image;
    private String n_of_like;
    private String caption;
    private Boolean owner;


}
